# optuna

[optuna](https://www.preferred.jp/ja/projects/optuna/)は、[㈱PFN](https://www.preferred.jp/ja/)が開発しているpyhtonベースのハイパーパラメータ自動最適化フレームワークです。ベイズ最適化アルゴリズムを利用しており、
- Define-by-Run スタイル
- Pruning (学習曲線を用いた試行の枝刈り)
- 並列分散最適化
- DBを用いて試行の再開などが容易  

といった特長があり、OpenFOAMなどと組み合わせた最適化計算等にも非常に使いやすいと考えます。 

"ハイパーパラメータ"とは人手で設定するようなパラメータの総称で、通常、計算をしたい人が手動で試行錯誤するかわりに、自動でパラメータ値を探索（ある条件や範囲で）するフレームワークの一つがoptunaです。  
現在、optunaは順調にバージョンアップし、ver.3.0.0(2022/09現在)です。  
ここでは備忘録的にその使い方をまとめ、最終的にOpenFOAMでの最適化計算との組み合わせでの実行例まで説明したいと思います。

## Install

非常に簡単です。なお、optuna ver.3.0.0は、python3.6以降で利用します。
[こちらにdocument](https://optuna.readthedocs.io/en/stable/index.html)があります。
以下の説明は、Ubuntu 22.04LTS（WSL2）での確認です（python3.10）。
```
pip3 install optuna
```

## お試し最適化（tutorial_1）

1次元の関数の最小値を求めるoptunaのpythonコードはざっくりと以下のようになります。

```
#### tutorial_1.py

import optuna   #optunaのインポート

def objective(trial):
  # 目的関数の定義
  x = trial.suggest_float("x", -5, 5) # 試すxを指定範囲から選ぶ (parameter suggestion) 
  f = 3*x**4 - 2*x**3 - 4*x**2 + 2 #４次関数（）
  return f

# studyインスタンスの作成と試行（トライアル）100回の実行
study = optuna.create_study()
study.optimize(objective, n_trials=100)

# 最適化結果の表示（最小値）
print(study.best_params)
print(study.best_value)
```

実行結果は以下の通りになります。
```
$ python3 tutorial_1.py
[snip]
[I 2022-09-07 09:04:13,047] Trial 95 finished with value: 7.667790030547533 and parameters: {'x': 1.765348180787555}. Best is trial 51 with value: -1.10935950461276.
[I 2022-09-07 09:04:13,049] Trial 96 finished with value: 1.1564107979850304 and parameters: {'x': 0.4430700288968277}. Best is trial 51 with value: -1.10935950461276.
[I 2022-09-07 09:04:13,051] Trial 97 finished with value: -1.1092941594736758 and parameters: {'x': 1.0967366890293577}. Best is trial 51 with value: -1.10935950461276.
[I 2022-09-07 09:04:13,052] Trial 98 finished with value: -0.24611874953285184 and parameters: {'x': 0.7737919007560563}. Best is trial 51 with value: -1.10935950461276.
[I 2022-09-07 09:04:13,054] Trial 99 finished with value: 365.9859713678891 and parameters: {'x': -3.2563921881818163}. Best is trial 51 with value: -1.10935950461276.
{'x': 1.1106243870106547}   # ←最小値を与えるxの値
-1.10935950461276  # ←関数の最小値
```

## [trial部分の説明](https://optuna.readthedocs.io/en/stable/reference/generated/optuna.trial.Trial.html)

関数の中で、変数xの探索範囲を指定しています。上記の例は、-5～5の範囲で連続的に探索（TPEアルゴリズムによる）してfloat値を設定するという意味になります。これ以外にint（整数値を設定）、categorical（選択肢を設定）などがあります。  
　（将来バージョンでなくなる予定の設定方法もありますのでご注意下さい。）

## [studyインスタンス作成部分の説明](https://optuna.readthedocs.io/en/latest/reference/generated/optuna.study.create_study.html#optuna.study.create_study)

study.create_study()には、様々なオプションがあります。指定しなければ、すべてdefault設定が適用されます。
```
study = optuna_create_study(
    storage=None,
    sampler=None, 
    pruner=None, 
    study_name=None, 
    direction=None, 
    load_if_exists=False, 
    directions=Non
)
```  
storage：RDBを使った分散並列最適化を実行するあるいは、DBに保存したデータを利用して最適化を再開する場合に利用します。  
sampler: [サンプリングアルゴリズム](https://optuna.readthedocs.io/en/latest/reference/samplers/index.html#module-optuna.samplers)を指定します。指定しなければTPEsamplerアルゴリズムが使われます。  
pruner: [枝かり（pruner）アルゴリズム](https://optuna.readthedocs.io/en/latest/reference/pruners.html#module-optuna.pruners)を指定します。最適化の効率化のためによく用いられます。  
study_name: studyの名前を指定します。指定しない場合は自動設定されます。  
direction： 最適化の方向（最大化、最小化）を設定します。  
load_if_exists: Trueの場合、study_nameをキーとして既存studyがあればそれを読み込みます。  
directions: directionと紛らわしいですが、同一のstudyに対してdirectionを変更したい場合にその列を並べて書きます。上述のdirectionとは両方併記できません。  

## CFD計算との連携例（tutorial_2）

上記のobjectの中身を、「ハイパーパラメータを渡してCFDを実行し、何らかの返り値を戻す」という作業に置き換えればOKです。  
この「CFDの計算結果から何らかの返り値を返す作業」は、pythonコードの中でも実行できますが、
切り分けたほうがわかりやすいと思います。

そのような実行シェルスクリプトを使って実行する一例として以下のような簡単な例を示します。  
この例では、実際にCFDを計算するわけではありませんが、選定したxを引数としてCFD計算を行い（＝実際には、xに対して4次関数の値を計算）、戻り値を返す外部のAllrunスクリプト(sh)を利用しています。

なおシェルスクリプトは、optunaのpythonコードからはsubprocessとして実行する必要があるようです。

tutorial_2.py
```
### tutorial_2.py

import subprocess as sp
import optuna   #optunaのインポート

def run_CFD(x): # CFD実行スクリプト呼び出し関数
   args = "sh ./Allrun " + str(x) # シェルに渡すコマンド文字列（今回は以下のAllrunスクリプトを引数付きで実行）
   proc = sp.Popen(args, shell=True, stdout=sp.PIPE, stderr=sp.PIPE) # コマンド文字列をサブプロセスでシェルに渡す
   line = float(proc.stdout.readline()) # str --> float #stdoutへの戻り値(byte)をfloatにキャストする
   return line

def objective(trial):
  x = trial.suggest_float("x", -5, 5) # 試すxを指定範囲から選ぶ (parameter suggestion)
  return run_CFD(x) # CFD実行スクリプト呼び出し関数を実行

# studyインスタンスの作成と試行（トライアル）100回の実行
study = optuna.create_study()
study.optimize(objective, n_trials=100)

# 最適化結果の表示（最小値）
print(study.best_params)
print(study.best_value)
```

Allrun (上記のargsでxを引数として渡して実行しているシェルスクリプトで、ここではxを使って4次関数を計算して出力しているだけです。)
```
#!/bin/sh

x=$(echo $1 | bc -l)
f=$(echo "scale=16;3.0*$x^4-2.0*$x^3-4.0*$x^2+2.0" | bc -l)
printf "%.8f" $f
```

## ローカルでの並列実行例（tutorial_3)

studyインスタンスを作成する際に、storageとstudy_nameを指定するだけでoptunaがSQLite DBファイルをつくってくれます。  
また、load_if_exists=Trueとしておくと、該当するDBファイルを読み込み、それまでの計算を継続することもできます。  

tutorial_3.py
```
### tutorial_3.py
import optuna   #optunaのインポート

def objective(trial):
  # 目的関数の定義
  x = trial.suggest_float("x", -5, 5) # 試すxを指定範囲から選ぶ (parameter suggestion) 
  f = 3*x**4 - 2*x**3 - 4*x**2 + 2 #４次関数（）
  return f

# studyインスタンスの作成と試行（トライアル）100回の実行
study = optuna.create_study(
  study_name='myStudy',
  storage='sqlite:///./myStudy.db',   
  load_if_exists=True
)
study.optimize(objective, n_trials=100)

# 最適化結果の表示（最小値）
print(study.best_params)
print(study.best_value)
```

できたdb(sqlite)の中身は、そのままでは見れませんが、
以下のようなpythonコード（SQLでdbファイルからデータを持ってきているだけですが）を利用して確認することもできます。
```
import pandas as pd
import sqlite3

### DB名は適宜変更すること↓
with sqlite3.connect("./myStudy.db") as conn:
    print("tables")
    df = pd.read_sql("SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';", conn)
    print(df)

    print("studies")
    df = pd.read_sql("SELECT * FROM studies;", conn)
    print(df)

    print("trials")
    df = pd.read_sql("SELECT * FROM trials;", conn)
    print(df)
```

実行結果は以下のようになります。
tablesのnameを見るとなんとなくわかると思いますが、それぞれの試行結果やパラメータなどは
trial_params, trial_valuesなどに含まれています。

```
tables
                         name
0                     studies
1                version_info
2            study_directions
3       study_user_attributes
4     study_system_attributes
5                      trials
6       trial_user_attributes
7     trial_system_attributes
8                trial_params
9                trial_values
10  trial_intermediate_values
11           trial_heartbeats
12            alembic_version
studies
   study_id study_name
0         1    myStudy
trials
    trial_id  number  study_id     state              datetime_start           datetime_complete
0          1       0         1  COMPLETE  2022-09-07 20:02:59.780689  2022-09-07 20:02:59.793789
1          2       1         1  COMPLETE  2022-09-07 20:02:59.806161  2022-09-07 20:02:59.814751
2          3       2         1  COMPLETE  2022-09-07 20:02:59.823454  2022-09-07 20:02:59.831733
3          4       3         1  COMPLETE  2022-09-07 20:02:59.840447  2022-09-07 20:02:59.849048
4          5       4         1  COMPLETE  2022-09-07 20:02:59.857581  2022-09-07 20:02:59.866014
..       ...     ...       ...       ...                         ...                         ...
95        96      95         1  COMPLETE  2022-09-07 20:03:01.778209  2022-09-07 20:03:01.787889
96        97      96         1  COMPLETE  2022-09-07 20:03:01.796401  2022-09-07 20:03:01.806460
97        98      97         1  COMPLETE  2022-09-07 20:03:01.814826  2022-09-07 20:03:01.824947
98        99      98         1  COMPLETE  2022-09-07 20:03:01.833945  2022-09-07 20:03:01.843794
99       100      99         1  COMPLETE  2022-09-07 20:03:01.852293  2022-09-07 20:03:01.861937

[100 rows x 6 columns]
```

## OpenFOAMとの連携例(tutorial_4)

OpenFOAMのtutorialを利用して、実際の連携例を示します。  
題材として、tutorial/simpleFoam/mixerVessel2Dを利用し、パッチ面rotorの発生する回転トルク（z軸方向）が最大になる回転数（constant/MRFProperties中のomega[rad/s]）を見つけるということを考えます。このモデルの場合、回転トルクはマイナスになるので(流体抵抗による)、
「最大値はomega=0[rad/s]のときに、回転トルク0」になります。物理的にはあまり意味のない計算ですがサンプル用なのでご容赦下さい。

### STEP1: mixerVessel2Dチュートリアルの変更
トルクを評価するためcontrolDictを修正してトルクを計算・出力するようにします。

（system/controlDictにfunctionObjectの設定を追記）
```
functions
{
  forces1
  {
    // Mandatory entries
    type            forces;
    libs            ("libforces.so");
    patches         ( rotor );

    // Field names
    p               p;
    U               U;
    rho             rhoInf;
    rhoInf          1;

    // Store and write volume field representations of forces and moments
    log             true;
    //writeFields     yes;
    writeControl    timeStep;
    timeInterval    1;

    // Centre of rotation for moment calculations
    CofR            (0 0 0);
    pitchAxis       (0 1 0);
  }
}
```
### STEP2: Allrunスクリプトの変更
次にオリジナルのAllrunスクリプトを改造し、sed, awkコマンドを利用して以下のような実行ができるようにします。

①Allcleanコマンドも最初に実行する  
②runFucntionのstdout, stderrを/dev/nullに捨てて余計なプロンプト出力をなくす  
③引数として回転数omegaをとるようにする  
④内部でconstant/MRFProperties中のomegaの値を書き換える（sedコマンド利用）  
⑤simpleFoamの計算後、postProcessingフォルダ中のmoment.datからz軸周りのモーメントを取り出し、コマンドラインに出力する（awkコマンド利用）  
 (本来は各trialでCFD計算が最後まで収束しているかどうかなどのチェックも必要ですが、今回は簡易に、一番最後のiterationでの値を取り出しています。)

Allrun（改造後）
```
#!/bin/sh
cd "${0%/*}" || exit                                # Run from this directory
. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions        # Tutorial run functions
#------------------------------------------------------------------------------

./Allclean > /dev/null 2>&1

restore0Dir > /dev/null 2>&1

m4 system/blockMeshDict.m4 > system/blockMeshDict
runApplication blockMesh > /dev/null 2>&1

omegaValue=$(echo $1 | bc -l)
sed -i "/omega/c  omega  $omegaValue ;"  constant/MRFProperties

runApplication $(getApplication) > /dev/null 2>&1

awk 'END{print $4}' postProcessing/forces1/0/moment.dat
#------------------------------------------------------------------------------
```

これを使って、最適化を実行するのは以下のスクリプトになります。


tutorial_4.py
```
import subprocess as sp
import optuna   #optunaのインポート

def run_CFD(omega): #CFD実行スクリプト呼び出し関数
   args = "sh ./Allrun " + str(omega) # シェルに渡すコマンド文字列（今回は以下のAllrunスクリプトを引数付きで実行）
   proc = sp.Popen(args, shell=True, stdout=sp.PIPE, stderr=sp.PIPE) # コマンド文字列をサブプロセスでシェルに渡す
   line = float(proc.stdout.readline()) # str --> float #stdoutへの戻り値(byte)をfloatにキャストする
   return line

def objective(trial):
  omega = trial.suggest_float("omega", 0, 200) # 試すxを指定範囲から選ぶ (parameter suggestion)
  return run_CFD(omega) # CFD実行スクリプト呼び出し関数を実行

# studyインスタンスの作成と試行（トライアル）100回の実行
study = optuna.create_study(direction = "maximize")
study.optimize(objective, n_trials=100)

# 最適化結果の表示
print(study.best_params)
print(study.best_value)
```
実行結果は以下の通りになり、omegaがほぼ0でトルク最大（ほぼ0)ということになっています。完全に0になっていないのは、trialの回数が少ないためかもしれません。
```
[I 2022-09-08 12:59:15,671] Trial 89 finished with value: -5.204275e-06 and parameters: {'omega': 21.475566422052133}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 12:59:20,652] Trial 90 finished with value: -1.064891e-06 and parameters: {'omega': 5.44980925976461}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 12:59:25,507] Trial 91 finished with value: -1.990448e-07 and parameters: {'omega': 0.8751728065852682}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 12:59:30,525] Trial 92 finished with value: -1.835497e-08 and parameters: {'omega': 0.07972580523312807}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 12:59:35,505] Trial 93 finished with value: -2.1705e-06 and parameters: {'omega': 11.958605383113353}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 12:59:40,392] Trial 94 finished with value: -3.009686e-06 and parameters: {'omega': 15.03279951309791}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 12:59:45,025] Trial 95 finished with value: -1.022302e-06 and parameters: {'omega': 5.145720194862763}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 12:59:49,766] Trial 96 finished with value: -0.0001043466 and parameters: {'omega': 107.43920991146345}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 12:59:54,602] Trial 97 finished with value: -1.280358e-05 and parameters: {'omega': 35.87739398804813}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 12:59:59,436] Trial 98 finished with value: -1.554156e-06 and parameters: {'omega': 9.365976328757425}. Best is trial 81 with value: -1.280494e-08.
[I 2022-09-08 13:00:04,380] Trial 99 finished with value: -9.689697e-08 and parameters: {'omega': 0.42309142585830656}. Best is trial 81 with value: -1.280494e-08.
{'omega': 0.055602969443571515} ← トルク最大となるomega[rad/s]
-1.280494e-08 ← トルク最大値
```
